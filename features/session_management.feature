Feature: Session management
  In order to get access to my todo application
  As a user
  I want to be properly authenticated

  Scenario: Successful login
    Given the user "aslan" has an account
    When he logs in
    Then he should see "List of todo tasks"
  Scenario: Authentication failure
    Given the user "aslan" does not have an account
    When he logs in
    Then he should see "Authentication failed"
  Scenario: Successful login followed by logout
    Given the user "aslan" has an account
    When he logs in
    And then he logs out
    Then he should see "You have been logged out"