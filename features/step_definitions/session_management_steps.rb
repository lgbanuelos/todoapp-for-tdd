Given(/^the user "(.*?)" has an account$/) do |username|
    @user = FactoryGirl.create(:user, username: username)
end

When(/^he logs in$/) do
    visit '/login'
    fill_in 'Username', with: @user.username
    fill_in 'Password', with: @user.password
    click_on 'Log in'
end

Then(/^he should see "(.*?)"$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

Given(/^the user "(.*?)" does not have an account$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

When(/^then he logs out$/) do
  pending # express the regexp above with the code you wish you had
end
