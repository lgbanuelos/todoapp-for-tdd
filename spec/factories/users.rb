FactoryGirl.define do
  factory :user do
    full_name "MyString"
    username "MyString"
    email "MyString"
    password "MyString"
    password_confirmation "MyString"
    confirmed_at "2015-10-20 07:44:11"
    role "MyString"
  end

end
